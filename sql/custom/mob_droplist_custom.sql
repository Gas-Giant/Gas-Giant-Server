-- ---------------------------------------------------------------------------
-- This file adds onto, modifies, or removes portions of the table "mob_droplist"
-- created by the file mob_droplist.sql and must be imported/executed AFTER that file.
--
-- For new entries the main trunk does not have, use "INSERT INTO"
-- For changing entries that already exist use "REPLACE INTO"
-- REPLACE tells MySQL to delete the old line and insert the new one.
-- Using the wrong 1 of these 2 commands will result in errors.
--
-- To REMOVE a drop that the main trunk has but you do not want:
-- DELETE FROM `mob_droplist` WHERE 'dropid'=0 and 'type'=0 and 'itemid'=0 'droprate'=0;
-- Where the zero is replaced by whatever values DarkStar was using.
--
-- Remember to comment what is dropping from what mob on each line: -- Item name from NM name
-- ---------------------------------------------------------------------------

-- Examples
-- INSERT INTO `mob_droplist` VALUES (dropid,type,itemid,droprate); -- Added Item_name to Mob_Name
-- REPLACE INTO `mob_droplist` VALUES (dropid,type,itemid,droprate); -- Added Item_name to Mob_Name
-- DELETE FROM `mob_droplist` WHERE 'dropid'=1 and 'type'=2 and 'itemid'=3 'droprate'=4; -- Removed Item_name from Mob_Name
-- New entries below this line, and sorted by dropid.
