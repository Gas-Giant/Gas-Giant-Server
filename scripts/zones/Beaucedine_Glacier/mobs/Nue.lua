-----------------------------------
--  Area: Beaucedine Glacier (111)
--  NM:	  Nue
-----------------------------------

require("scripts/globals/settings");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)	

    -- Set Nue's Window Open Time
    local wait = (math.random(3600,7200) /SHORT_NM_TIMER_MOD);
    SetServerVariable("[POP]Nue", os.time(t) + wait); -- 1-2 hours
    DeterMob(mob:getID(), true);
    
    -- Set PH back to normal, then set to respawn spawn
    PH = GetServerVariable("[PH]Nue");
    SetServerVariable("[PH]Nue", 0);
    DeterMob(PH, false);
    GetMobByID(PH):setRespawnTime(GetMobRespawnTime(PH));

end;

