-----------------------------------
--  Area: Beaucedine Glacier (111)
--  NM:  Gargantua
-----------------------------------

require("scripts/globals/quests");
require("scripts/globals/settings");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)	

    -- Set Gargantua's Window Open Time
    local wait = (math.random(3600,25200) /SHORT_NM_TIMER_MOD);
    SetServerVariable("[POP]Gargantua", os.time(t) + wait); -- 1-7 hours
    DeterMob(mob:getID(), true);
    
    -- Set PH back to normal, then set to respawn spawn
    PH = GetServerVariable("[PH]Gargantua");
    SetServerVariable("[PH]Gargantua", 0);
    DeterMob(PH, false);
    GetMobByID(PH):setRespawnTime(GetMobRespawnTime(PH));

    local DALEQuest = player:getVar("DALEQuest");
    if(player:getQuestStatus(JEUNO,THE_MIRACULOUS_DALE) == QUEST_ACCEPTED) then
        DALEQuest = player:setMaskBit(DALEQuest,"DALEQuest",8,true);
    end
end;

