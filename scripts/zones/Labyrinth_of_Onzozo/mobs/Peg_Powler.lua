----------------------------------
-- Area: Labyrinth of Onzozo
--   NM: Peg Powler
-----------------------------------

require("scripts/globals/groundsofvalor");
require("scripts/globals/settings");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)

    checkGoVregime(killer,mob,774,1);

    -- Set Peg Powler's Window Open Time
    local wait = (math.random(7200,57600) /LONG_NM_TIMER_MOD);
    SetServerVariable("[POP]Peg_Powler", os.time(t) + wait); -- 2-16 hours
    DeterMob(mob:getID(), true);

    -- Set PH back to normal, then set to respawn spawn
    local PH = GetServerVariable("[PH]Peg_Powler");
    SetServerVariable("[PH]Peg_Powler", 0);
    DeterMob(PH, false);
    GetMobByID(PH):setRespawnTime(GetMobRespawnTime(PH));

end;