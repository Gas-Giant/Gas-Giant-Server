----------------------------------
-- Area: Labyrinth of Onzozo
--   NM: Narasimha
-----------------------------------

require("scripts/globals/settings");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)

    -- Set Narasimha's Window Open Time
    local wait = (math.random(21600,36000) /SHORT_NM_TIMER_MOD); -- 6-10 hours
    SetServerVariable("[POP]Narasimha", os.time(t) + wait);
    DeterMob(mob:getID(), true);

    -- Set PH back to normal, then set to respawn spawn
    local PH = GetServerVariable("[PH]Narasimha");
    SetServerVariable("[PH]Narasimha", 0);
    DeterMob(PH, false);
    GetMobByID(PH):setRespawnTime(GetMobRespawnTime(PH));

end;