-----------------------------------	
-- Area: Pashhow Marshlands	
-- MOB:  Jolly Green	
-----------------------------------	
	
require("scripts/globals/fieldsofvalor");	
require("scripts/globals/quests");
	
-----------------------------------	
-- onMobDeath	
-----------------------------------	
	
function onMobDeath(mob,killer)	
	checkRegime(killer,mob,60,3);
    local DALEQuest = player:getVar("DALEQuest");
    if(player:getQuestStatus(JEUNO,THE_MIRACULOUS_DALE) == QUEST_ACCEPTED) then
        DALEQuest = player:setMaskBit(DALEQuest,"DALEQuest",10,true);
    end
end;	
