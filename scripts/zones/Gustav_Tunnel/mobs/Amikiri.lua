----------------------------------	
-- Area: Gustav Tunnel
--   NM: Amikiri
-----------------------------------	
  
require("scripts/globals/settings");

-----------------------------------	
-- onMobDeath	
-----------------------------------	
	
function onMobDeath(mob,killer)	
  
    -- Set Amikiri's Window Open Time
    local wait = (math.random(25200,32400) /SHORT_NM_TIMER_MOD);
    SetServerVariable("[POP]Amikiri", os.time(t) + wait); -- 7-9 hours
    DeterMob(mob:getID(), true);

    -- Set PH back to normal, then set to respawn spawn
    local PH = GetServerVariable("[PH]Amikiri");
    SetServerVariable("[PH]Amikiri", 0);
    DeterMob(PH, false);
    GetMobByID(PH):setRespawnTime(GetMobRespawnTime(PH));
  
end;