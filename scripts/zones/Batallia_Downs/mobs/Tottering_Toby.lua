-----------------------------------
--  Area: Batallia Downs (105)
--   Mob: Tottering_Toby
-----------------------------------

require("scripts/globals/quests");
require("scripts/globals/settings");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)	

    -- Set Tottering_Toby's Window Open Time
    local wait = (math.random(3600,14400) /SHORT_NM_TIMER_MOD);
    SetServerVariable("[POP]Tottering_Toby", os.time(t) + wait); -- 1-4 hours
    DeterMob(mob:getID(), true);
    
    -- Set PH back to normal, then set to respawn spawn
    PH = GetServerVariable("[PH]Tottering_Toby");
    SetServerVariable("[PH]Tottering_Toby", 0);
    DeterMob(PH, false);
    GetMobByID(PH):setRespawnTime(GetMobRespawnTime(PH));

    local DALEQuest = killer:getVar("DALEQuest");
    if(killer:getQuestStatus(JEUNO,THE_MIRACULOUS_DALE) == QUEST_ACCEPTED) then
        DALEQuest = killer:setMaskBit(DALEQuest,"DALEQuest",14,true);
    end

end;

