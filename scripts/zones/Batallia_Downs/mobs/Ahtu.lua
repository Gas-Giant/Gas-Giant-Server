-----------------------------------
--  Area: Batallia Downs (105)
--   Mob: Ahtu
-----------------------------------

require("scripts/globals/settings");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)	

    -- Set Ahtu's spawnpoint and respawn time (2-4 hours)
    UpdateNMSpawnPoint(mob:getID());
    mob:setRespawnTime(math.random(7200,14400) /SHORT_NM_TIMER_MOD);

end;

