-----------------------------------
-- Area: Fei'Yin
-- NM:   Capricious_Cassie
-----------------------------------

require("scripts/globals/titles");
require("scripts/globals/settings");

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function onMobSpawn(mob)
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob, killer)

    killer:addTitle(CASSIENOVA);

    -- Set Capricious_Cassie's spawnpoint and respawn time (21-24 hours)
    UpdateNMSpawnPoint(mob:getID());
    mob:setRespawnTime(math.random(75600,86400) /LONG_NM_TIMER_MOD);

end;