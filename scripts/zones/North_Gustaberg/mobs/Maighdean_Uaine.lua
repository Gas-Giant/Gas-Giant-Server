----------------------------------	
-- Area: North Gustaberg
-- NM: 	 Maighdean Uaine
-----------------------------------	

require("scripts/globals/fieldsofvalor");	
require("scripts/globals/settings");

-----------------------------------	
-- onMobDeath	
-----------------------------------	
	
function onMobDeath(mob,killer)	

    -- Set Maighdean_Uaine's Window Open Time
    local wait = (math.random(900,10800) /SHORT_NM_TIMER_MOD)
    SetServerVariable("[POP]Maighdean_Uaine", os.time(t) + wait); -- 15-180 minutes
    DeterMob(mob:getID(), true);

    -- Set PH back to normal, then set to respawn spawn
    PH = GetServerVariable("[PH]Maighdean_Uaine");
    SetServerVariable("[PH]Maighdean_Uaine", 0);
    DeterMob(PH, false);
    GetMobByID(PH):setRespawnTime(GetMobRespawnTime(PH));
  
end;