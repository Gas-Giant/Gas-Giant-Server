-----------------------------------
--  Area: Castle Zvahl Baileys (161)
--    NM: Marquis_Amon
-----------------------------------

require("scripts/globals/settings");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)	

    -- Set Marquis_Amon's spawnpoint and respawn time (21-24 hours)
    UpdateNMSpawnPoint(mob:getID());
    mob:setRespawnTime(math.random(75600,86400) /LONG_NM_TIMER_MOD);

end;

