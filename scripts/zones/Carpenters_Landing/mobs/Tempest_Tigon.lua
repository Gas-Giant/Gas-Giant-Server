-----------------------------------
-- Area: Carpenter's Landing
--  NM:  Tempest Tigon
-- @pos -237.025 -4.946 -294.933 2
-----------------------------------

require("scripts/globals/settings");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)	

    -- Set Tempest Tigon's spawnpoint and respawn time (1-2 hours)
    UpdateNMSpawnPoint(mob:getID());
    mob:setRespawnTime(math.random(3600,7200) /SHORT_NM_TIMER_MOD);

end;