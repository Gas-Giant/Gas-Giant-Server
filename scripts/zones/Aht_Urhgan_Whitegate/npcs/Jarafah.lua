-----------------------------------
-- Area: Aht Urhgan Whitegate
--  NPC: Jarafah
-- Type: Item Depository (Event Item Storage NPC)
-- @pos 16.945 0.000 -16.432 50
-----------------------------------
-- Items:
-- player:startEvent(701, Furnishings1, Weapons/Shields, Armor-Head, Armor, 0, Furnishings2);
-- Furnishings 1             ALL = 2147483647
-- 1. Sandorian Holiday Tree                1
-- 2. Bastokan Holiday Tree                 2
-- 3. Windurstian Holiday Tree              4
-- 4. Kadomatsu                             8
-- 5. Wing Egg                             16
-- 6. Lamp Egg                             32
-- 7. Flower Egg                           64
-- 8. Adventuring Certificate             128
-- 9. Timepiece                           256
-- 10. Miniature Airship                  512
-- 11. Pumpkin Lantern                   1024
-- 12. Bomb Lantern                      2048
-- 13. Mandragora Lantern                4096
-- 14. Dream Platter                     8192
-- 15. Dream Coffer                     16384
-- 16. Dream Stocking                   32768
-- 17. Copy of Hoary Spire              65536
-- 18. Jeweled Egg                     131072
-- 19. Sprig of Red Bamboo Grass       262144
-- 20. Sprig of Blue Bamboo Grass      524288
-- 21. Sprig of Green Bamboo Grass    1048576
-- 22. Snowman Knight                 2097152
-- 23. Snowman Miner                  4194304
-- 24. Snowman Mage                   8388608
-- 25. Bonbori                       16777216
-- 26. Set of Festive Dolls          33554432
-- 27. Melodious Egg                 67108864
-- 28. Clockwork Egg                134217728
-- 29. Hatchling Egg                268435456
-- 30. Harpsichord                  536870912
-- 31. Aldebaran Horn              1073741824
--
-- Furnishings 2               ALL = 16777215
-- 1. Stuffed Chocobo                       1
-- 2. Egg Buffet                            2
-- 3. Adamantoise Statue                    4
-- 4. Behemoth Statue                       8
-- 5. Fafnir Statue                        16
-- 6. Pepo Lantern                         32
-- 7. Cushaw Lantern                       64
-- 8. Calabazilla Lantern                 128
-- 9. Junoan Tree                         256
-- 10. Shadow Lord Statue                 512
-- 11. Kabuto-kazari                     1024
-- 12. Katana-kazari                     2048
-- 13. Odin Statue                       4096
-- 14. Alexander Statue                  8192
-- 15. Carillon Vermeil                 16384
-- 16. Aeolsglockes                     32768
-- 17. Leafbells                        65536
-- 18. San d'Orian Flag                131072
-- 19. Bastokan Flag                   262144
-- 20. Windurstian Flag                524288
-- 21. Jack-o'-pricket                1048576
-- 22. Djinn Pricket                  2097152
-- 23. Korrigan Pricket               4194304
-- 24. Mandragora Pricket             8388608
--
-- Weapons & Shields            ALL = 1048575
-- 1. Chocobo Wand                          1
-- 2. Trick Staff                           2
-- 3. Treat Staff                           4
-- 4. Treat Staff II                        8
-- 5. Wooden Katana                        16
-- 6. Hardwood Katana                      32
-- 7. Pitchfork                            64
-- 8. Pitchfork +1                        128
-- 9. Charm Wand +1                       256
-- 10. Lotus Katana                       512
-- 11. Moogle Rod                        1024
-- 12. Battledore                        2048
-- 13. Miracle Wand +1                   4096
-- 14. Shinai                            8192
-- 15. Ibushi Shinai                    16384
-- 16. Ibushi Shinai +1                 32768
-- 17. Town Moogle Shield               65536
-- 18. Nomad Moogle Shield             131072
-- 19. Dream Bell                      262144
-- 20. Dream Bell +1                   524288
--
-- Armor Helmets                 ALL = 524287
-- 1. Pumpkin Head                          1
-- 2. Horror Head                           2
-- 3. Pumpkin Head II                       4
-- 4. Horror Head II                        8
-- 5. Dream Hat                            16
-- 6. Dream Hat +1                         32
-- 7. Sprout Beret                         64
-- 8. Guide Beret                         128
-- 9. Mandragora Beret                    256
-- 10. Witch Hat                          512
-- 11. Coven Hat                         1024
-- 12. Egg Helm                          2048
-- 13. Moogle Cap                        4096
-- 14. Nomad Cap                         8192
-- 15. Pair of Redeyes                  16384
-- 16. Sol Cap                          32768
-- 17. Lunar Cap                        65536
-- 18. Snow Bunny Hat +1               131072
-- 19. Chocobo Beret                   262144
--
-- Armor Other                  ALL = 8388607
-- 1. Onoko Yukata                          1
-- 2. Lords Yukata                          2
-- 3. Hume Gilet                            4
-- 4. Hume Gilet +1                         8
-- 5. Pair of Hume Trunks                  16
-- 6. Pair of Hume Trunks +1               32
-- 7. Dream Robe                           64
-- 8. Dream Robe +1                       128
-- 9. Otoko Yukata                        256
-- 10. Otokogimi Yukata                   512
-- 11. Pair of Dream Boots               1024
-- 12. Pair of Dream Boots +1            2048
-- 13. Custom Gilet                      4096
-- 14. Custom Gilet +1                   8192
-- 15. Pair of Custom Trunks            16384
-- 16. Pair of Custom Trunks +1         32768
-- 17. Eerie Cloak                      65536
-- 18. Eerie Cloak +1                  131072
-- 19. Tidal Talisman                  262144
-- 20. Otokogusa Yukata                524288
-- 21. Otokoeshi Yukata               1048576
-- 22. Dinner Jacket                  2097152
-- 23. Pair of Dinner Boots           4194304
-----------------------------------
package.loaded["scripts/zones/Aht_Urhgan_Whitegate/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/settings");
require("scripts/zones/Aht_Urhgan_Whitegate/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
    local count = trade:getItemCount();
    local eventItemsStored1 = player:getVar("eventItemsStored1");
    local eventItemsStored2 = player:getVar("eventItemsStored2");
    local eventItemsStored3 = player:getVar("eventItemsStored3");
    local eventItemsStored4 = player:getVar("eventItemsStored4");
    local eventItemsStored5 = player:getVar("eventItemsStored5");

    if (ALL_EVENT_ITEMS_STORED == 1) then
        eventItemsStored1 = 2147483647;
        eventItemsStored2 = 1048575;
        eventItemsStored3 = 524287;
        eventItemsStored4 = 8388607;
        eventItemsStored5 = 16777215;
    end

    if (trade:hasItemQty(86,1) == true and count == 1) then
        eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",0,true);
        player:startEvent(701, 86);
    elseif (trade:hasItemQty(115,1) == true and count == 1) then
        eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",1,true);
        player:startEvent(701, 115);
    elseif (trade:hasItemQty(116,1) == true and count == 1) then
        eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",2,true);
        player:startEvent(701, 116);
    elseif (trade:hasItemQty(87,1) == true and count == 1) then
        eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",3,true);
        player:startEvent(701, 87);
    elseif (trade:hasItemQty(117,1) == true and count == 1) then
        eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",4,true);
        player:startEvent(701, 117);
    elseif (trade:hasItemQty(118,1) == true and count == 1) then
        eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",5,true);
        player:startEvent(701, 118);
    elseif (trade:hasItemQty(119,1) == true and count == 1) then
        eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",6,true);
        player:startEvent(701, 119);
    elseif (trade:hasItemQty(193,1) == true and count == 1) then
        eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",7,true);
        player:startEvent(701, 193);
    elseif (trade:hasItemQty(88,1) == true and count == 1) then
        eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",8,true);
        player:startEvent(701, 88);
    elseif (trade:hasItemQty(154,1) == true and count == 1) then
        eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",9,true);
        player:startEvent(701, 154);
    elseif (trade:hasItemQty(204,1) == true and count == 1) then
        eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",10,true);
        player:startEvent(701, 204);
    elseif (trade:hasItemQty(203,1) == true and count == 1) then
        eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",11,true);
        player:startEvent(701, 203);
    elseif (trade:hasItemQty(205,1) == true and count == 1) then
        eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",12,true);
        player:startEvent(701, 205);
    elseif (trade:hasItemQty(140,1) == true and count == 1) then
        eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",13,true);
        player:startEvent(701, 140);
    elseif (trade:hasItemQty(141,1) == true and count == 1) then
        eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",14,true);
        player:startEvent(701, 141);
    elseif (trade:hasItemQty(155,1) == true and count == 1) then
        eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",15,true);
        player:startEvent(701, 155);
    elseif (trade:hasItemQty(192,1) == true and count == 1) then
        eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",16,true);
        player:startEvent(701, 192);
    elseif (trade:hasItemQty(179,1) == true and count == 1) then
        eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",17,true);
        player:startEvent(701, 179);
    elseif (trade:hasItemQty(323,1) == true and count == 1) then
        eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",18,true);
        player:startEvent(701, 323);
    elseif (trade:hasItemQty(324,1) == true and count == 1) then
        eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",19,true);
        player:startEvent(701, 324);
    elseif (trade:hasItemQty(325,1) == true and count == 1) then
        eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",20,true);
        player:startEvent(701, 325);
    elseif (trade:hasItemQty(176,1) == true and count == 1) then
        eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",21,true);
        player:startEvent(701, 176);
    elseif (trade:hasItemQty(177,1) == true and count == 1) then
        eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",22,true);
        player:startEvent(701, 177);
    elseif (trade:hasItemQty(178,1) == true and count == 1) then
        eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",23,true);
        player:startEvent(701, 178);
    elseif (trade:hasItemQty(180,1) == true and count == 1) then
        eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",24,true);
        player:startEvent(701, 180);
    elseif (trade:hasItemQty(215,1) == true and count == 1) then
        eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",25,true);
        player:startEvent(701, 215);
    elseif (trade:hasItemQty(196,1) == true and count == 1) then
        eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",26,true);
        player:startEvent(701, 196);
    elseif (trade:hasItemQty(197,1) == true and count == 1) then
        eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",27,true);
        player:startEvent(701, 197);
    elseif (trade:hasItemQty(199,1) == true and count == 1) then
        eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",28,true);
        player:startEvent(701, 199);
    elseif (trade:hasItemQty(320,1) == true and count == 1) then
        eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",29,true);
        player:startEvent(701, 320);
    elseif (trade:hasItemQty(415,1) == true and count == 1) then
        eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",30,true);
        player:startEvent(701, 415);
    elseif (trade:hasItemQty(17074,1) == true and count == 1) then
        eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",0,true);
        player:startEvent(701, 17074);
    elseif (trade:hasItemQty(17565,1) == true and count == 1) then
        eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",1,true);
        player:startEvent(701, 17565);
    elseif (trade:hasItemQty(17566,1) == true and count == 1) then
        eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",2,true);
        player:startEvent(701, 17566);
    elseif (trade:hasItemQty(17588,1) == true and count == 1) then
        eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",3,true);
        player:startEvent(701, 17588);
    elseif (trade:hasItemQty(17830,1) == true and count == 1) then
        eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",4,true);
        player:startEvent(701, 17830);
    elseif (trade:hasItemQty(17831,1) == true and count == 1) then
        eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",5,true);
        player:startEvent(701, 17831);
    elseif (trade:hasItemQty(18102,1) == true and count == 1) then
        eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",6,true);
        player:startEvent(701, 18102);
    elseif (trade:hasItemQty(18103,1) == true and count == 1) then
        eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",7,true);
        player:startEvent(701, 18103);
    elseif (trade:hasItemQty(18400,1) == true and count == 1) then
        eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",8,true);
        player:startEvent(701, 18400);
    elseif (trade:hasItemQty(18436,1) == true and count == 1) then
        eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",9,true);
        player:startEvent(701, 18436);
    elseif (trade:hasItemQty(18401,1) == true and count == 1) then
        eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",10,true);
        player:startEvent(701, 18401);
    elseif (trade:hasItemQty(18846,1) == true and count == 1) then
        eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",11,true);
        player:startEvent(701, 18846);
    elseif (trade:hasItemQty(18845,1) == true and count == 1) then
        eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",12,true);
        player:startEvent(701, 18845);
    elseif (trade:hasItemQty(18441,1) == true and count == 1) then
        eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",13,true);
        player:startEvent(701, 18441);
    elseif (trade:hasItemQty(17748,1) == true and count == 1) then
        eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",14,true);
        player:startEvent(701, 17748);
    elseif (trade:hasItemQty(17749,1) == true and count == 1) then
        eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",15,true);
        player:startEvent(701, 17749);
    elseif (trade:hasItemQty(16182,1) == true and count == 1) then
        eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",16,true);
        player:startEvent(701, 16182);
    elseif (trade:hasItemQty(16183,1) == true and count == 1) then
        eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",17,true);
        player:startEvent(701, 16183);
    elseif (trade:hasItemQty(18863,1) == true and count == 1) then
        eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",18,true);
        player:startEvent(701, 18863);
    elseif (trade:hasItemQty(18864,1) == true and count == 1) then
        eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",19,true);
        player:startEvent(701, 18864);
    elseif (trade:hasItemQty(13916,1) == true and count == 1) then
        eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",0,true);
        player:startEvent(701, 13916);
    elseif (trade:hasItemQty(13917,1) == true and count == 1) then
        eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",1,true);
        player:startEvent(701, 13917);
    elseif (trade:hasItemQty(15176,1) == true and count == 1) then
        eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",2,true);
        player:startEvent(701, 15176);
    elseif (trade:hasItemQty(15177,1) == true and count == 1) then
        eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",3,true);
        player:startEvent(701, 15177);
    elseif (trade:hasItemQty(15178,1) == true and count == 1) then
        eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",4,true);
        player:startEvent(701, 15178);
    elseif (trade:hasItemQty(15179,1) == true and count == 1) then
        eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",5,true);
        player:startEvent(701, 15179);
    elseif (trade:hasItemQty(15198,1) == true and count == 1) then
        eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",6,true);
        player:startEvent(701, 15198);
    elseif (trade:hasItemQty(15199,1) == true and count == 1) then
        eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",7,true);
        player:startEvent(701, 15199);
    elseif (trade:hasItemQty(15204,1) == true and count == 1) then
        eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",8,true);
        player:startEvent(701, 15204);
    elseif (trade:hasItemQty(16075,1) == true and count == 1) then
        eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",9,true);
        player:startEvent(701, 16075);
    elseif (trade:hasItemQty(16076,1) == true and count == 1) then
        eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",10,true);
        player:startEvent(701, 16076);
    elseif (trade:hasItemQty(16109,1) == true and count == 1) then
        eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",11,true);
        player:startEvent(701, 16109);
    elseif (trade:hasItemQty(16118,1) == true and count == 1) then
        eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",12,true);
        player:startEvent(701, 16118);
    elseif (trade:hasItemQty(16119,1) == true and count == 1) then
        eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",13,true);
        player:startEvent(701, 16119);
    elseif (trade:hasItemQty(16120,1) == true and count == 1) then
        eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",14,true);
        player:startEvent(701, 16120);
    elseif (trade:hasItemQty(16144,1) == true and count == 1) then
        eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",15,true);
        player:startEvent(701, 16144);
    elseif (trade:hasItemQty(16145,1) == true and count == 1) then
        eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",16,true);
        player:startEvent(701, 16145);
    elseif (trade:hasItemQty(11491,1) == true and count == 1) then
        eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",17,true);
        player:startEvent(701, 11491);
    elseif (trade:hasItemQty(11500,1) == true and count == 1) then
        eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",18,true);
        player:startEvent(701, 11500);
    elseif (trade:hasItemQty(13819,1) == true and count == 1) then
        eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",0,true);
        player:startEvent(701, 13819);
    elseif (trade:hasItemQty(13821,1) == true and count == 1) then
        eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",1,true);
        player:startEvent(701, 13821);
    elseif (trade:hasItemQty(14450,1) == true and count == 1) then
        eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",2,true);
        player:startEvent(701, 14450);
    elseif (trade:hasItemQty(14457,1) == true and count == 1) then
        eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",3,true);
        player:startEvent(701, 14457);
    elseif (trade:hasItemQty(15408,1) == true and count == 1) then
        eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",4,true);
        player:startEvent(701, 15408);
    elseif (trade:hasItemQty(15415,1) == true and count == 1) then
        eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",5,true);
        player:startEvent(701, 15415);
    elseif (trade:hasItemQty(14519,1) == true and count == 1) then
        eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",6,true);
        player:startEvent(701, 14519);
    elseif (trade:hasItemQty(14520,1) == true and count == 1) then
        eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",7,true);
        player:startEvent(701, 14520);
    elseif (trade:hasItemQty(14532,1) == true and count == 1) then
        eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",8,true);
        player:startEvent(701, 14532);
    elseif (trade:hasItemQty(14534,1) == true and count == 1) then
        eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",9,true);
        player:startEvent(701, 14534);
    elseif (trade:hasItemQty(15752,1) == true and count == 1) then
        eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",10,true);
        player:startEvent(701, 15752);
    elseif (trade:hasItemQty(15753,1) == true and count == 1) then
        eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",11,true);
        player:startEvent(701, 15753);
    elseif (trade:hasItemQty(11265,1) == true and count == 1) then
        eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",12,true);
        player:startEvent(701, 11265);
    elseif (trade:hasItemQty(11273,1) == true and count == 1) then
        eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",13,true);
        player:startEvent(701, 11273);
    elseif (trade:hasItemQty(16321,1) == true and count == 1) then
        eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",14,true);
        player:startEvent(701, 16321);
    elseif (trade:hasItemQty(16329,1) == true and count == 1) then
        eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",15,true);
        player:startEvent(701, 16329);
    elseif (trade:hasItemQty(11300,1) == true and count == 1) then
        eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",16,true);
        player:startEvent(701, 11300);
    elseif (trade:hasItemQty(11301,1) == true and count == 1) then
        eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",17,true);
        player:startEvent(701, 11301);
    elseif (trade:hasItemQty(11290,1) == true and count == 1) then
        eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",18,true);
        player:startEvent(701, 11290);
    elseif (trade:hasItemQty(11316,1) == true and count == 1) then
        eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",19,true);
        player:startEvent(701, 11316);
    elseif (trade:hasItemQty(11318,1) == true and count == 1) then
        eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",20,true);
        player:startEvent(701, 11318);
    elseif (trade:hasItemQty(11355,1) == true and count == 1) then
        eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",21,true);
        player:startEvent(701, 11355);
    elseif (trade:hasItemQty(16378,1) == true and count == 1) then
        eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",22,true);
        player:startEvent(701, 16378);
    elseif (trade:hasItemQty(264,1) == true and count == 1) then
        eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",0,true);
        player:startEvent(701, 264);
    elseif (trade:hasItemQty(455,1) == true and count == 1) then
        eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",1,true);
        player:startEvent(701, 455);
    elseif (trade:hasItemQty(265,1) == true and count == 1) then
        eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",2,true);
        player:startEvent(701, 265);
    elseif (trade:hasItemQty(266,1) == true and count == 1) then
        eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",3,true);
        player:startEvent(701, 266);
    elseif (trade:hasItemQty(267,1) == true and count == 1) then
        eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",4,true);
        player:startEvent(701, 267);
    elseif (trade:hasItemQty(456,1) == true and count == 1) then
        eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",5,true);
        player:startEvent(701, 456);
    elseif (trade:hasItemQty(457,1) == true and count == 1) then
        eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",6,true);
        player:startEvent(701, 457);
    elseif (trade:hasItemQty(458,1) == true and count == 1) then
        eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",7,true);
        player:startEvent(701, 458);
    elseif (trade:hasItemQty(138,1) == true and count == 1) then
        eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",8,true);
        player:startEvent(701, 138);
    elseif (trade:hasItemQty(269,1) == true and count == 1) then
        eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",9,true);
        player:startEvent(701, 269);
    elseif (trade:hasItemQty(3641,1) == true and count == 1) then
        eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",10,true);
        player:startEvent(701, 3641);
    elseif (trade:hasItemQty(3642,1) == true and count == 1) then
        eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",11,true);
        player:startEvent(701, 3642);
    elseif (trade:hasItemQty(270,1) == true and count == 1) then
        eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",12,true);
        player:startEvent(701, 270);
    elseif (trade:hasItemQty(271,1) == true and count == 1) then
        eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",13,true);
        player:startEvent(701, 271);
    elseif (trade:hasItemQty(3643,1) == true and count == 1) then
        eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",14,true);
        player:startEvent(701, 3643);
    elseif (trade:hasItemQty(3644,1) == true and count == 1) then
        eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",15,true);
        player:startEvent(701, 3644);
    elseif (trade:hasItemQty(3645,1) == true and count == 1) then
        eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",16,true);
        player:startEvent(701, 3645);
    elseif (trade:hasItemQty(181,1) == true and count == 1) then
        eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",17,true);
        player:startEvent(701, 181);
    elseif (trade:hasItemQty(182,1) == true and count == 1) then
        eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",18,true);
        player:startEvent(701, 182);
    elseif (trade:hasItemQty(183,1) == true and count == 1) then
        eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",19,true);
        player:startEvent(701, 183);
    elseif (trade:hasItemQty(3622,1) == true and count == 1) then
        eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",20,true);
        player:startEvent(701, 3622);
    elseif (trade:hasItemQty(3623,1) == true and count == 1) then
        eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",21,true);
        player:startEvent(701, 3623);
    elseif (trade:hasItemQty(3624,1) == true and count == 1) then
        eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",22,true);
        player:startEvent(701, 3624);
    elseif (trade:hasItemQty(3646,1) == true and count == 1) then
        eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",23,true);
        player:startEvent(701, 3646);
    end
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
    local eventItemsStored1 = player:getVar("eventItemsStored1");
    local eventItemsStored2 = player:getVar("eventItemsStored2");
    local eventItemsStored3 = player:getVar("eventItemsStored3");
    local eventItemsStored4 = player:getVar("eventItemsStored4");
    local eventItemsStored5 = player:getVar("eventItemsStored5");

    if (ALL_EVENT_ITEMS_STORED == 1) then
        eventItemsStored1 = 2147483647;
        eventItemsStored2 = 1048575;
        eventItemsStored3 = 524287;
        eventItemsStored4 = 8388607;
        eventItemsStored5 = 16777215;
    end

    player:startEvent(702, eventItemsStored1, eventItemsStored2, eventItemsStored3, eventItemsStored4, 0, eventItemsStored5);
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
    -- printf("CSID: %u",csid);
    -- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
    -- printf("CSID: %u",csid);
    -- printf("RESULT: %u",option);
    local eventItemsStored1 = player:getVar("eventItemsStored1");
    local eventItemsStored2 = player:getVar("eventItemsStored2");
    local eventItemsStored3 = player:getVar("eventItemsStored3");
    local eventItemsStored4 = player:getVar("eventItemsStored4");
    local eventItemsStored5 = player:getVar("eventItemsStored5");

    if (ALL_EVENT_ITEMS_STORED == 1) then
        eventItemsStored1 = 2147483647;
        eventItemsStored2 = 1048575;
        eventItemsStored3 = 524287;
        eventItemsStored4 = 8388607;
        eventItemsStored5 = 16777215;
    end

    if (csid == 701) then
        player:tradeComplete();
    elseif (csid == 702 and option ~= 1073741825) then
        if (player:getFreeSlotsCount(0) >= 1) then
            player:delGil(250);
            if (option == 0) then
                eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",0,false);
                player:addItem(86);
                player:messageSpecial(ITEM_OBTAINED, 86);
            elseif (option == 1) then
                eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",1,false);
                player:addItem(115);
                player:messageSpecial(ITEM_OBTAINED, 115);
            elseif (option == 2) then
                eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",2,false);
                player:addItem(116);
                player:messageSpecial(ITEM_OBTAINED, 116);
            elseif (option == 3) then
                eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",3,false);
                player:addItem(87);
                player:messageSpecial(ITEM_OBTAINED, 87);
            elseif (option == 4) then
                eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",4,false);
                player:addItem(117);
                player:messageSpecial(ITEM_OBTAINED, 117);
            elseif (option == 5) then
                eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",5,false);
                player:addItem(118);
                player:messageSpecial(ITEM_OBTAINED, 118);
            elseif (option == 6) then
                eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",6,false);
                player:addItem(119);
                player:messageSpecial(ITEM_OBTAINED, 119);
            elseif (option == 7) then
                eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",7,false);
                player:addItem(193);
                player:messageSpecial(ITEM_OBTAINED, 193);
            elseif (option == 8) then
                eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",8,false);
                player:addItem(88);
                player:messageSpecial(ITEM_OBTAINED, 88);
            elseif (option == 9) then
                eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",9,false);
                player:addItem(154);
                player:messageSpecial(ITEM_OBTAINED, 154);
            elseif (option == 10) then
                eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",10,false);
                player:addItem(204);
                player:messageSpecial(ITEM_OBTAINED, 204);
            elseif (option == 11) then
                eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",11,false);
                player:addItem(203);
                player:messageSpecial(ITEM_OBTAINED, 203);
            elseif (option == 12) then
                eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",12,false);
                player:addItem(205);
                player:messageSpecial(ITEM_OBTAINED, 205);
            elseif (option == 13) then
                eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",13,false);
                player:addItem(140);
                player:messageSpecial(ITEM_OBTAINED, 140);
            elseif (option == 14) then
                eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",14,false);
                player:addItem(141);
                player:messageSpecial(ITEM_OBTAINED, 141);
            elseif (option == 15) then
                eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",15,false);
                player:addItem(155);
                player:messageSpecial(ITEM_OBTAINED, 155);
            elseif (option == 64) then
                eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",16,false);
                player:addItem(192);
                player:messageSpecial(ITEM_OBTAINED, 192);
            elseif (option == 65) then
                eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",17,false);
                player:addItem(179);
                player:messageSpecial(ITEM_OBTAINED, 179);
            elseif (option == 66) then
                eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",18,false);
                player:addItem(323);
                player:messageSpecial(ITEM_OBTAINED, 323);
            elseif (option == 67) then
                eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",19,false);
                player:addItem(324);
                player:messageSpecial(ITEM_OBTAINED, 324);
            elseif (option == 68) then
                eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",20,false);
                player:addItem(325);
                player:messageSpecial(ITEM_OBTAINED, 325);
            elseif (option == 69) then
                eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",21,false);
                player:addItem(176);
                player:messageSpecial(ITEM_OBTAINED, 176);
            elseif (option == 70) then
                eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",22,false);
                player:addItem(177);
                player:messageSpecial(ITEM_OBTAINED, 177);
            elseif (option == 71) then
                eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",23,false);
                player:addItem(178);
                player:messageSpecial(ITEM_OBTAINED, 178);
            elseif (option == 72) then
                eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",24,false);
                player:addItem(180);
                player:messageSpecial(ITEM_OBTAINED, 180);
            elseif (option == 73) then
                eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",25,false);
                player:addItem(215);
                player:messageSpecial(ITEM_OBTAINED, 215);
            elseif (option == 74) then
                eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",26,false);
                player:addItem(196);
                player:messageSpecial(ITEM_OBTAINED, 196);
            elseif (option == 75) then
                eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",27,false);
                player:addItem(197);
                player:messageSpecial(ITEM_OBTAINED, 197);
            elseif (option == 76) then
                eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",28,false);
                player:addItem(199);
                player:messageSpecial(ITEM_OBTAINED, 199);
            elseif (option == 77) then
                eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",29,false);
                player:addItem(320);
                player:messageSpecial(ITEM_OBTAINED, 320);
            elseif (option == 78) then
                eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",30,false);
                player:addItem(415);
                player:messageSpecial(ITEM_OBTAINED, 415);
            elseif (option == 16) then
                eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",0,false);
                player:addItem(17074);
                player:messageSpecial(ITEM_OBTAINED, 17074);
            elseif (option == 17) then
                eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",1,false);
                player:addItem(17565);
                player:messageSpecial(ITEM_OBTAINED, 17565);
            elseif (option == 18) then
                eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",2,false);
                player:addItem(17566);
                player:messageSpecial(ITEM_OBTAINED, 17566);
            elseif (option == 19) then
                eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",3,false);
                player:addItem(17588);
                player:messageSpecial(ITEM_OBTAINED, 17588);
            elseif (option == 20) then
                eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",4,false);
                player:addItem(17830);
                player:messageSpecial(ITEM_OBTAINED, 17830);
            elseif (option == 21) then
                eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",5,false);
                player:addItem(17831);
                player:messageSpecial(ITEM_OBTAINED, 17831);
            elseif (option == 22) then
                eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",6,false);
                player:addItem(18102);
                player:messageSpecial(ITEM_OBTAINED, 18102);
            elseif (option == 23) then
                eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",7,false);
                player:addItem(18103);
                player:messageSpecial(ITEM_OBTAINED, 18103);
            elseif (option == 24) then
                eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",8,false);
                player:addItem(18400);
                player:messageSpecial(ITEM_OBTAINED, 18400);
            elseif (option == 25) then
                eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",9,false);
                player:addItem(18436);
                player:messageSpecial(ITEM_OBTAINED, 18436);
            elseif (option == 26) then
                eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",10,false);
                player:addItem(18401);
                player:messageSpecial(ITEM_OBTAINED, 18401);
            elseif (option == 27) then
                eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",11,false);
                player:addItem(18846);
                player:messageSpecial(ITEM_OBTAINED, 18846);
            elseif (option == 28) then
                eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",12,false);
                player:addItem(18845);
                player:messageSpecial(ITEM_OBTAINED, 18845);
            elseif (option == 29) then
                eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",13,false);
                player:addItem(18441);
                player:messageSpecial(ITEM_OBTAINED, 18441);
            elseif (option == 30) then
                eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",14,false);
                player:addItem(17748);
                player:messageSpecial(ITEM_OBTAINED, 17748);
            elseif (option == 31) then
                eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",15,false);
                player:addItem(17749);
                player:messageSpecial(ITEM_OBTAINED, 17749);
            elseif (option == 92) then
                eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",16,false);
                player:addItem(16182);
                player:messageSpecial(ITEM_OBTAINED, 16182);
            elseif (option == 93) then
                eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",17,false);
                player:addItem(16183);
                player:messageSpecial(ITEM_OBTAINED, 16183);
            elseif (option == 94) then
                eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",18,false);
                player:addItem(18863);
                player:messageSpecial(ITEM_OBTAINED, 18863);
            elseif (option == 95) then
                eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",19,false);
                player:addItem(18864);
                player:messageSpecial(ITEM_OBTAINED, 18864);
            elseif (option == 32) then
                eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",0,false);
                player:addItem(13916);
                player:messageSpecial(ITEM_OBTAINED, 13916);
            elseif (option == 33) then
                eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",1,false);
                player:addItem(13917);
                player:messageSpecial(ITEM_OBTAINED, 13917);
            elseif (option == 34) then
                eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",2,false);
                player:addItem(15176);
                player:messageSpecial(ITEM_OBTAINED, 15176);
            elseif (option == 35) then
                eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",3,false);
                player:addItem(15177);
                player:messageSpecial(ITEM_OBTAINED, 15177);
            elseif (option == 36) then
                eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",4,false);
                player:addItem(15178);
                player:messageSpecial(ITEM_OBTAINED, 15178);
            elseif (option == 37) then
                eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",5,false);
                player:addItem(15179);
                player:messageSpecial(ITEM_OBTAINED, 15179);
            elseif (option == 38) then
                eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",6,false);
                player:addItem(15198);
                player:messageSpecial(ITEM_OBTAINED, 15198);
            elseif (option == 39) then
                eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",7,false);
                player:addItem(15199);
                player:messageSpecial(ITEM_OBTAINED, 15199);
            elseif (option == 40) then
                eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",8,false);
                player:addItem(15204);
                player:messageSpecial(ITEM_OBTAINED, 15204);
            elseif (option == 41) then
                eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",9,false);
                player:addItem(16075);
                player:messageSpecial(ITEM_OBTAINED, 16075);
            elseif (option == 42) then
                eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",10,false);
                player:addItem(16076);
                player:messageSpecial(ITEM_OBTAINED, 16076);
            elseif (option == 43) then
                eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",11,false);
                player:addItem(16109);
                player:messageSpecial(ITEM_OBTAINED, 16109);
            elseif (option == 44) then
                eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",12,false);
                player:addItem(16118);
                player:messageSpecial(ITEM_OBTAINED, 16118);
            elseif (option == 45) then
                eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",13,false);
                player:addItem(16119);
                player:messageSpecial(ITEM_OBTAINED, 16119);
            elseif (option == 46) then
                eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",14,false);
                player:addItem(16120);
                player:messageSpecial(ITEM_OBTAINED, 16120);
            elseif (option == 47) then
                eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",15,false);
                player:addItem(16144);
                player:messageSpecial(ITEM_OBTAINED, 16144);
            elseif (option == 80) then
                eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",16,false);
                player:addItem(16145);
                player:messageSpecial(ITEM_OBTAINED, 16145);
            elseif (option == 81) then
                eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",17,false);
                player:addItem(11491);
                player:messageSpecial(ITEM_OBTAINED, 11419);
            elseif (option == 82) then
                eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",18,false);
                player:addItem(11500);
                player:messageSpecial(ITEM_OBTAINED, 11500);
            elseif (option == 48) then
                eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",0,false);
                player:addItem(13819);
                player:messageSpecial(ITEM_OBTAINED, 13819);
            elseif (option == 49) then
                eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",1,false);
                player:addItem(13821);
                player:messageSpecial(ITEM_OBTAINED, 13821);
            elseif (option == 50) then
                eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",2,false);
                player:addItem(14450);
                player:messageSpecial(ITEM_OBTAINED, 14450);
            elseif (option == 51) then
                eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",3,false);
                player:addItem(14457);
                player:messageSpecial(ITEM_OBTAINED, 14457);
            elseif (option == 52) then
                eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",4,false);
                player:addItem(15408);
                player:messageSpecial(ITEM_OBTAINED, 15408);
            elseif (option == 53) then
                eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",5,false);
                player:addItem(15415);
                player:messageSpecial(ITEM_OBTAINED, 15415);
            elseif (option == 54) then
                eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",6,false);
                player:addItem(14519);
                player:messageSpecial(ITEM_OBTAINED, 14519);
            elseif (option == 55) then
                eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",7,false);
                player:addItem(14520);
                player:messageSpecial(ITEM_OBTAINED, 14520);
            elseif (option == 56) then
                eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",8,false);
                player:addItem(14532);
                player:messageSpecial(ITEM_OBTAINED, 14532);
            elseif (option == 57) then
                eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",9,false);
                player:addItem(14534);
                player:messageSpecial(ITEM_OBTAINED, 14534);
            elseif (option == 58) then
                eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",10,false);
                player:addItem(15752);
                player:messageSpecial(ITEM_OBTAINED, 15752);
            elseif (option == 59) then
                eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",11,false);
                player:addItem(15753);
                player:messageSpecial(ITEM_OBTAINED, 15753);
            elseif (option == 60) then
                eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",12,false);
                player:addItem(11265);
                player:messageSpecial(ITEM_OBTAINED, 11265);
            elseif (option == 61) then
                eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",13,false);
                player:addItem(11273);
                player:messageSpecial(ITEM_OBTAINED, 11273);
            elseif (option == 62) then
                eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",14,false);
                player:addItem(16321);
                player:messageSpecial(ITEM_OBTAINED, 16321);
            elseif (option == 63) then
                eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",15,false);
                player:addItem(16329);
                player:messageSpecial(ITEM_OBTAINED, 16329);
            elseif (option == 84) then
                eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",16,false);
                player:addItem(11300);
                player:messageSpecial(ITEM_OBTAINED, 11300);
            elseif (option == 85) then
                eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",17,false);
                player:addItem(11301);
                player:messageSpecial(ITEM_OBTAINED, 11301);
            elseif (option == 86) then
                eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",18,false);
                player:addItem(11290);
                player:messageSpecial(ITEM_OBTAINED, 11290);
            elseif (option == 87) then
                eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",19,false);
                player:addItem(11316);
                player:messageSpecial(ITEM_OBTAINED, 11316);
            elseif (option == 88) then
                eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",20,false);
                player:addItem(11318);
                player:messageSpecial(ITEM_OBTAINED, 11318);
            elseif (option == 126) then
                eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",21,false);
                player:addItem(11355);
                player:messageSpecial(ITEM_OBTAINED, 11355);
            elseif (option == 127) then
                eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",22,false);
                player:addItem(16378);
                player:messageSpecial(ITEM_OBTAINED, 16378);
            elseif (option == 96) then
                eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",0,false);
                player:addItem(264);
                player:messageSpecial(ITEM_OBTAINED, 264);
            elseif (option == 97) then
                eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",1,false);
                player:addItem(455);
                player:messageSpecial(ITEM_OBTAINED, 455);
            elseif (option == 98) then
                eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",2,false);
                player:addItem(265);
                player:messageSpecial(ITEM_OBTAINED, 265);
            elseif (option == 99) then
                eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",3,false);
                player:addItem(266);
                player:messageSpecial(ITEM_OBTAINED, 266);
            elseif (option == 100) then
                eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",4,false);
                player:addItem(267);
                player:messageSpecial(ITEM_OBTAINED, 267);
            elseif (option == 101) then
                eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",5,false);
                player:addItem(456);
                player:messageSpecial(ITEM_OBTAINED, 456);
            elseif (option == 102) then
                eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",6,false);
                player:addItem(457);
                player:messageSpecial(ITEM_OBTAINED, 457);
            elseif (option == 103) then
                eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",7,false);
                player:addItem(458);
                player:messageSpecial(ITEM_OBTAINED, 458);
            elseif (option == 104) then
                eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",8,false);
                player:addItem(138);
                player:messageSpecial(ITEM_OBTAINED, 138);
            elseif (option == 105) then
                eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",9,false);
                player:addItem(269);
                player:messageSpecial(ITEM_OBTAINED, 269);
            elseif (option == 106) then
                eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",10,false);
                player:addItem(3641);
                player:messageSpecial(ITEM_OBTAINED, 3641);
            elseif (option == 107) then
                eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",11,false);
                player:addItem(3642);
                player:messageSpecial(ITEM_OBTAINED, 3642);
            elseif (option == 108) then
                eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",12,false);
                player:addItem(207);
                player:messageSpecial(ITEM_OBTAINED, 207);
            elseif (option == 109) then
                eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",13,false);
                player:addItem(271);
                player:messageSpecial(ITEM_OBTAINED, 271);
            elseif (option == 110) then
                eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",14,false);
                player:addItem(3643);
                player:messageSpecial(ITEM_OBTAINED, 3643);
            elseif (option == 111) then
                eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",15,false);
                player:addItem(3644);
                player:messageSpecial(ITEM_OBTAINED, 3644);
            elseif (option == 112) then
                eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",16,false);
                player:addItem(3645);
                player:messageSpecial(ITEM_OBTAINED, 3645);
            elseif (option == 113) then
                eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",17,false);
                player:addItem(181);
                player:messageSpecial(ITEM_OBTAINED, 181);
            elseif (option == 114) then
                eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",18,false);
                player:addItem(182);
                player:messageSpecial(ITEM_OBTAINED, 182);
            elseif (option == 115) then
                eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",19,false);
                player:addItem(183);
                player:messageSpecial(ITEM_OBTAINED, 183);
            elseif (option == 116) then
                eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",20,false);
                player:addItem(3622);
                player:messageSpecial(ITEM_OBTAINED, 3622);
            elseif (option == 117) then
                eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",21,false);
                player:addItem(3623);
                player:messageSpecial(ITEM_OBTAINED, 3623);
            elseif (option == 118) then
                eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",22,false);
                player:addItem(3624);
                player:messageSpecial(ITEM_OBTAINED, 3624);
            elseif (option == 119) then
                eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",23,false);
                player:addItem(3646);
                player:messageSpecial(ITEM_OBTAINED, 3646);
            end
        end
    end
end;