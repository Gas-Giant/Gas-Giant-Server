-----------------------------------
-- Area: Ordelles Caves (193)
-- NPC:  Morbolger
-----------------------------------

require("scripts/globals/titles");
require("scripts/globals/settings");

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function onMobSpawn(mob)
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob, killer)

	killer:addTitle(MORBOLBANE);

    -- Set Morbolger's spawnpoint and respawn time (21-24 hours)
    UpdateNMSpawnPoint(mob:getID());
    mob:setRespawnTime(math.random(75600,86400) /LONG_NM_TIMER_MOD);

end;