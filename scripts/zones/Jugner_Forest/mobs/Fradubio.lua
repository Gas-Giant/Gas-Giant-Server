----------------------------------	
-- Area: Jugner_Forest
-- NM:   Fradubio
-- @pos 76.573 -0.901 -246.241 104
-----------------------------------	

require("scripts/globals/settings");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)	

    -- Set Fradubio spawnpoint and respawn time (21-24 hours)
    UpdateNMSpawnPoint(mob:getID());
    mob:setRespawnTime(math.random(75600,86400) /LONG_NM_TIMER_MOD);

end;