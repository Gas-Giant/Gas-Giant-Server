----------------------------------	
-- Area: Kuftal Tunnel	
--   NM: Amemet
-- ToDo: Amemet should walk in a big circle
-----------------------------------	
  
require("scripts/globals/settings");

-----------------------------------	
-- onMobDeath	
-----------------------------------	
	
function onMobDeath(mob,killer)	

    -- Set Amemet's Window Open Time
    local wait = (math.random(7200,43200) /LONG_NM_TIMER_MOD); -- 2-12 hours
    SetServerVariable("[POP]Amemet", os.time(t) + wait);
    DeterMob(mob:getID(), true);

    -- Set PH back to normal, then set to respawn spawn
    local PH = GetServerVariable("[PH]Amemet");
    SetServerVariable("[PH]Amemet", 0);
    DeterMob(PH, false);
    GetMobByID(PH):setRespawnTime(GetMobRespawnTime(PH));
  
end;